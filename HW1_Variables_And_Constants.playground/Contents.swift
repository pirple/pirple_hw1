// Song Details Generator
// This file is going to store a list of attributes that a song will have. Some will be a required field and others optional. After storing this information it will be formatted and outputed to the console.

/*
Homework Assignment #1: Variables and Constants
Assignment requirements: Now, within that file, list all of the attributes of the song, (one after another on separate lines), by creating variables for each attribute, and giving each variable a value.
    Now, below your declarations, print the value of each variable to the console, one after another.
    When you're done, export the playground file, and push it up to Github
    Extra Credit: For extra credit, add comments to your code, that explain the different attributes. Also add a title to the top of your file explaining what the file is, and what it's for.
*/

/*
    Meta-Data of a song: (List of attibutes from this song)
    The list will contain: song name, artist, album, genre, year, track (#, of #), rating, bpm, play count, comments, length, bit rate kbps, channels, key, and format
*/

import UIKit

// assign values to the require fields first
// required fields are: song name, arist name, genre, year, length, bpm, bit rate kbps, channels, and formatt
let songName : String = "Don't Think Twice, It's All Right";
let aristName : String = "Bob Dylan";
let genre : String = "Folk Rock";
let year : Int = 1963;
let songLength : String = "3:40";
let bpm : Double = 110.00; // beats per minute can be 109.5 or 109.05 depending on the song so bpm must be a double
let bitRate : Int = 320;
let channel : String = "Stereo";
let formatt : String = "MP3";


// assign values to optional fields (fields that can be nil)
// optional fields: album, track #, rating, play count, comments, key
let album : String? = "Blowin' in the Wind";
let trackNumber : Int? = 7;
var rating : Int? = 5;
var playCount : Int? = 0;
//formatting comments to make it look nice in the console
var comments : String? = """
This song was released in August of 1963;
                   howevever, Bob Dylan wrote and recorded it on November 14 1962.
                   Label released on: Columbia Records
""";

// just for this assignment all keys will be label with one character
// Major keys will be uppercase and minor keys wil be lowercase
// ex. Key: E Major == "E"  && E Minor == "e"
let key : Character? = "E"

/*
    Assignment Notes: "Now, below your declarations, print the value of each variable to the console, one after another."
 
    States each variable needs to be printed out to the console one after another, does not state that each variable must be in its individual print statement. Each variable will be output in its own seperate line using a mutliLineString.
*/



// creating a mutliLineString will all this information:
var metaData : String = """
    Song Information:
    ====================================================
    Song Name:     \(songName)
    Artist Name:   \(aristName)
    Album Name:    \(album ?? "N/A")
    Year:          \(year)
    Genre:         \(genre)
    Song Duration: \(songLength)
    BPM:           \(bpm)
    Key:           \(key ?? " ")
    Track Number:  \(trackNumber ?? 1)
    Bitrate:       \(bitRate)
    Channel:       \(channel)
    Formatt:       \(formatt)
    Rating:        \(rating ?? 0)
    Play Count:    \(playCount ?? 0)
    Comments:      \(comments ?? " ")
    ====================================================
""";

// printing to the console.
print(metaData);

